<?php

namespace Demo\DemoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Demo\CommonBundle\Controller\GlobalController;

use Demo\DemoBundle\Entity\Person;
use Demo\DemoBundle\Form\PersonType;

class DemoController extends GlobalController
{
    // Repos
    protected $personRepo;

    // Entities
    protected $personClass = 'Demo\DemoBundle\Entity\Person';

    // Forms
    protected $personForm = 'Demo\DemoBundle\Form\PersonType';

    // Views
    protected $indexView = 'DemoBundle:Demo:index.html.twig';
    protected $createView ='DemoBundle:Person:new.html.twig'; 
    protected $editView ='DemoBundle:Person:edit.html.twig'; 

    public function initialize()
    {
        $this->personRepo = $this->em->getRepository('DemoBundle:Person');
    }

    public function indexAction(Request $request)
    {
        $people = $this->personRepo->findAllOrderByDate();

        $data = array(
            'page_name' => 'Home Page',        
            'people' => $people,
        );

        return $this->render(
            $this->indexView, 
            $data + $this->viewData()
        );

    }

    /**
     * Creates a new Person entity.
     *
     */
    public function createPersonAction(Request $request)
    {
        $person = new $this->personClass;
        $form = $this->createForm(new $this->personForm, $person);
        $flash = $this->container->get('session')->getFlashBag();

        if ($request->isMethod('post')) {

            $form->bind($request);

            if ($form->isValid()) {

                $this->em->persist($person);
                $this->em->flush();

                $flash->add('success','Person created successfully!');

                $redirect = $this->container->get('router')->generate('person_edit', array(
                    'id' => $person->getId(),
                ));

                return new RedirectResponse($redirect);
            }
        }

        $data = array(
            'entity' => $person,
            'form'   => $form->createView(),
        );

        return $this->render(
            $this->createView,
            $this->viewData() + $data
        );
    }



    /**
     * Displays a form to edit an existing Person entity.
     *
     */
    public function editPersonAction(Request $request, $id)
    {
        $entity = $this->personRepo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }

        $form = $this->createForm(new $this->personForm, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'entity'      => $entity,
            'edit_form'        => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );

        return $this->render(
            $this->editView,
            $this->viewData() + $data
        );
    }

    /**
     * Edits an existing Person entity.
     *
     */
    public function updatePersonAction(Request $request, $id)
    {
        $flash = $this->container->get('session')->getFlashBag();
        $entity = $this->personRepo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $form = $this->createForm(new $this->personForm, $entity);

        $form->bind($request);


        if ($form->isValid()) {
            $this->em->persist($entity);
            $this->em->flush();

            $flash->add('success', 'Person Updated successfully!');

            $redirect = $this->container->get('router')->generate('person_edit', array(
                'id' => $entity->getId(),
            ));

            return new RedirectResponse($redirect);

        }

        $data = array(
            'entity'      => $entity,
            'edit_form'        => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );

        return $this->render(
            $this->editView,
            $this->viewData() + $data
        );
    }

    /**
     * Deletes a Person entity.
     *
     */
    public function deletePersonAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        $flash = $this->container->get('session')->getFlashBag();

        if ($form->isValid()) {
            $entity = $this->personRepo->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Person entity.');
            }

            $this->em->remove($entity);
            $this->em->flush();

            $flash->add('success', 'Person deleted successfully!');
        }

        $redirect = $this->container->get('router')->generate('home');
        return new RedirectResponse($redirect);

    }


}
