<?php

namespace Demo\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SeoMetaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('route', 'hidden')
            ->add('seoTitle', 'text', array(
                'label' => 'SEO Title',            
                'required' => false,
            ))
            ->add('seoKeywords', 'text', array(
                'label' => 'SEO Keywords',            
                'required' => false,
            ))
            ->add('seoDescription', 'textarea', array(
                'label' => 'SEO Description',
                'required' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Demo\AdminBundle\Entity\SeoMeta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'demo_adminbundle_seometa';
    }
}
