<?php

namespace Demo\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RouteMetaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $types = $this->getTypes();
        $builder
            ->add('route', 'hidden')
            ->add('metaName', 'text', array(
                'label' => 'Meta Name',
                'required' => false,
            ))
            ->add('metaValue', 'ckeditor', array(
                'label' => 'Meta Value',
                'required' => false,
            ))
            ->add('metaType', 'choice', array(
                'label' => 'Meta Type',
                'choices' => $types,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Demo\AdminBundle\Entity\RouteMeta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'demo_adminbundle_routemeta';
    }

    private function  getTypes()
    {
        return array(
            'text' => 'text',
            'textarea' => 'textarea',
            'file' => 'file',
            'image' => 'image',
            'wysiwyg' => 'wysiwyg',
            'repeater' => 'repeater',
            'video' => 'video',
        );
    }

}
