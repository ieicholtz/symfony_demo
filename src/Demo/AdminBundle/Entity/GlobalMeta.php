<?php

namespace Demp\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlobalMeta
 *
 * @ORM\Table(name="global_meta")
 * @ORM\Entity(repositoryClass="Demo\AdminBundle\Entity\GlobalMetaRepository")
 */
class GlobalMeta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="section", type="string", length=255)
     */
    private $section;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_name", type="string", length=255, nullable=true)
     */
    private $metaName;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_value", type="text", nullable=true)
     */
    private $metaValue;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_type", type="string", length=255, nullable=true)
     */
    private $metaType;

    /**
     * @var integer
     *
     * @ORM\Column(name="field_order", type="integer")
     */
    private $fieldOrder;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set section
     *
     * @param string $section
     * @return GlobalMeta
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return string 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set metaName
     *
     * @param string $metaName
     * @return GlobalMeta
     */
    public function setMetaName($metaName)
    {
        $this->metaName = $metaName;

        return $this;
    }

    /**
     * Get metaName
     *
     * @return string 
     */
    public function getMetaName()
    {
        return $this->metaName;
    }

    /**
     * Set metaValue
     *
     * @param string $metaValue
     * @return GlobalMeta
     */
    public function setMetaValue($metaValue)
    {
        $this->metaValue = $metaValue;

        return $this;
    }

    /**
     * Get metaValue
     *
     * @return string 
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }

    /**
     * Set metaType
     *
     * @param string $metaType
     * @return GlobalMeta
     */
    public function setMetaType($metaType)
    {
        $this->metaType = $metaType;

        return $this;
    }

    /**
     * Get metaType
     *
     * @return string 
     */
    public function getMetaType()
    {
        return $this->metaType;
    }

    /**
     * Set fieldOrder
     *
     * @param integer $fieldOrder
     * @return GlobalMeta
     */
    public function setFieldOrder($fieldOrder)
    {
        $this->fieldOrder = $fieldOrder;

        return $this;
    }

    /**
     * Get fieldOrder
     *
     * @return integer 
     */
    public function getFieldOrder()
    {
        return $this->fieldOrder;
    }
}
