<?php

namespace Demp\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RouteMeta
 *
 * @ORM\Table(name="route_meta")
 * @ORM\Entity(repositoryClass="Demo\AdminBundle\Entity\RouteMetaRepository")
 */
class RouteMeta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=255)
     */
    private $route;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_name", type="string", length=255, nullable=true)
     */
    private $metaName;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_value", type="text", nullable=true)
     */
    private $metaValue;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_type", type="string", length=255, nullable=true)
     */
    private $metaType;

    /**
     * @var int
     *
     * @ORM\Column(name="field_order", type="integer", nullable=true)
     */
    private $fieldOrder;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return RouteMeta
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set metaName
     *
     * @param string $metaName
     * @return RouteMeta
     */
    public function setMetaName($metaName)
    {
        $this->metaName = $metaName;

        return $this;
    }

    /**
     * Get metaName
     *
     * @return string 
     */
    public function getMetaName()
    {
        return $this->metaName;
    }

    /**
     * Set metaValue
     *
     * @param string $metaValue
     * @return RouteMeta
     */
    public function setMetaValue($metaValue)
    {
        $this->metaValue = $metaValue;

        return $this;
    }

    /**
     * Get metaValue
     *
     * @return string 
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }

    /**
     * Set metaType
     *
     * @param string $metaType
     * @return RouteMeta
     */
    public function setMetaType($metaType)
    {
        $this->metaType = $metaType;

        return $this;
    }

    /**
     * Get metaType
     *
     * @return string 
     */
    public function getMetaType()
    {
        return $this->metaType;
    }

    /**
     * Set fieldOrder
     *
     * @param integer $fieldOrder
     * @return RouteMeta
     */
    public function setFieldOrder($fieldOrder)
    {
        $this->fieldOrder = $fieldOrder;

        return $this;
    }

    /**
     * Get fieldOrder
     *
     * @return integer 
     */
    public function getFieldOrder()
    {
        return $this->fieldOrder;
    }
}
