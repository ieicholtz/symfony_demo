<?php

namespace Demo\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Demo\CommonBundle\Controller\GlobalController;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormError;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Demo\AdminBundle\Entity\RouteMeta;
use Demo\AdminBundle\Form\RouteMetaType;

class RouteMetaController extends GlobalController
{
    // Repos
    private $routeRepo;

    // Routes
    protected $indexRoute           = 'admin_routes';
    protected $newRoute             = 'admin_routes_new';
    protected $editRoute            = 'admin_routes_edit';
    protected $updateRoute          = 'admin_routes_update';
    protected $deleteRoute          = 'admin_routes_delete';
    protected $viewRoute;
    protected $seoRoute;
    protected $reorderRoute         = 'admin_routes_reorder';
    
    // Naming
    protected $entityName           = 'Route Meta';
    protected $entitySingularName   = 'Route Meta';
    protected $entitySelector       = 'routes-overview';

    // Views
    protected $indexView            = 'AdminBundle:RouteMeta:index.html.twig';
    protected $newView              = 'AdminBundle:RouteMeta:new.html.twig';
    protected $editView             = 'AdminBundle:RouteMeta:edit.html.twig';
    
    // Classes
    protected $formClass            = 'Demo\AdminBundle\Form\RouteMetaType';
    protected $entityClass          = 'Demo\AdminBundle\Entity\RouteMeta';

    public function initialize()
    {
        $this->routeRepo = $this->em->getRepository('AdminBundle:RouteMeta');
    }

    /**
     * Index Action
     *
     */
    public function indexAction()
    {
        $routesList = $this->container->get('router')->getRouteCollection()->all();
        $entities = $this->routeRepo->findAllOrderByRouteAndOrder();
        $routes = array();
        foreach ($routesList as $route => $params) {
            if (preg_match('/admin_|^_|^fos|^acme/', $route)) continue;

                $routes[] = array(
                    'name' => $route,
                    'path' => $params->getPath(),
                );
        }
        $data = array(
            'routes' => $routes,
            'entities' => $entities,
        );

        $adminData = $this->adminViewData();
        return $this->render(
            $this->indexView,         
            $adminData + $data     
        );
    }

    /**
     * New Action
     *
     */
    public function newAction(Request $request, $route)
    {
        $routesList = $this->container->get('router')->getRouteCollection()->all();
        $routes = array();
        foreach ($routesList as $getRoute => $params) {
            if (preg_match('/cms_|^_|^fos|^acme/', $getRoute)) continue;

                $routes[] = array(
                    'name' => $getRoute,
                    'path' => $params->getPath(),
                );
        }
        $entity  = new $this->entityClass;
        $entity->setRoute($route);
        $form = $this->createForm(new $this->formClass, $entity);
        $flash = $this->container->get('session')->getFlashBag();

        if ($request->isMethod('post')) {

            $form->bind($request);

            if ($form->isValid()) {
                $count = $this->routeRepo->getNumberOfMetas();
                $rank = $count + 1;

                $entity->setFieldOrder($rank);

                $this->em->persist($entity);
                $this->em->flush();

                $flash->add('success', $this->entitySingularName.' created successfully!');

                $redirect = $this->container->get('router')->generate($this->editRoute, array(
                    'id' => $entity->getId(),
                ));

                return new RedirectResponse($redirect);
            }
        }

        $adminData = $this->adminViewData();
        $data = array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'route'  => $route,
        );

        return $this->render(
            $this->newView,         
            $adminData + $data     
        );
    }


    /**
     * Edit Action
     *
     */
    public function editAction(Request $request, $id)
    {
        $entity = $this->routeRepo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$this->entitySingularName.' entity.');
        }

        $route = $entity->getRoute();

        $form = $this->createForm(new $this->formClass, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $adminData = $this->adminViewData();
        $data = array(
            'route' => $route,
            'entity'      => $entity,
            'form'        => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );

        return $this->render(
            $this->editView,
            $adminData + $data
        );
    }

    /**
     * Update Action
     *
     */
    public function updateAction(Request $request, $id)
    {
        $flash = $this->container->get('session')->getFlashBag();
        $entity = $this->routeRepo->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$this->entitySingularName.' entity.');
        }

        $route = $entity->getRoute();

        $deleteForm = $this->createDeleteForm($id);
        $form = $this->createForm(new $this->formClass, $entity);

        $form->bind($request);

        if ($form->isValid()) {

            $this->em->persist($entity);
            $this->em->flush();

            $flash->add('success', $this->entitySingularName.' Updated successfully!');

            $redirect = $this->container->get('router')->generate($this->editRoute, array(
                'id' => $entity->getId(),
            ));

            return new RedirectResponse($redirect);

        }

        $adminData = $this->adminViewData();
        $data = array(
            'route' => $route,
            'entity'      => $entity,
            'form'        => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );

        return $this->render(
            $this->editView,
            $adminData + $data
        );
    }

    /**
     * Delete Action
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);
        $flash = $this->container->get('session')->getFlashBag();

        if ($form->isValid()) {
            $entity = $this->routeRepo->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find '.$this->entitySingularName.' entity.');
            }

            $this->em->remove($entity);
            $this->em->flush();

            $flash->add('success', $this->entitySingularName.' deleted successfully!');
        }
        $redirect = $this->container->get('router')->generate($this->indexRoute);
        return new RedirectResponse($redirect);
    }



    /**
     * Reorder Action
     *
     */
    public function reorderAction(Request $request)
    {
        $repo = $this->routeRepo;
        $order = $request->request->get('routes-overview');

        foreach ($order as $fieldOrder => $id) {
            $entity = $repo->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find '.$this->entitySingularName.' entity.');
            }

            $entity->setFieldOrder($fieldOrder + 1);
        }

        $this->em->flush();

        return new Response();
    }


}
