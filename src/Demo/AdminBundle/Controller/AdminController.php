<?php

namespace Demo\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Demo\CommonBundle\Controller\GlobalController;

use Demo\AdminBundle\Entity\SeoMeta;
use Demo\AdminBundle\Entity\RouteMeta;

use Demo\AdminBundle\Form\SeoMetaType;

class AdminController extends GlobalController
{
    protected $routeManager;

    // Repos
    private $seoRepo;
    private $routeRepo;

    // Views
    protected $indexView    = 'AdminBundle:Admin:index.html.twig';
    protected $contentView    = 'AdminBundle:Admin:content.html.twig';

    public function initialize()
    {
        $this->routeManager  = $this->container->get('demo.routeManager');
        $this->seoRepo = $this->em->getRepository('AdminBundle:SeoMeta');
        $this->routeRepo = $this->em->getRepository('AdminBundle:RouteMeta');
    }


    public function indexAction(Request $request)
    {
       /* $route = $request->get('_route');

        $limit = 5;

        $catering = $this->cateringRepo->findBy(array(), array('posted' => 'DESC'), $limit);
        $loyalty = $this->loyaltyRepo->findBy(array(), array('posted' => 'DESC'), $limit);
        $contact = $this->contactRepo->findBy(array(), array('posted' => 'DESC'), $limit);*/

        $data = array(
            /*'catering' => $catering,           
            'loyalty' => $loyalty,           
            'contact' => $contact,*/
        );

        return $this->render(
            $this->indexView, 
            $data
        );

    }

    public function contentSeoAction(Request $request)
    {
        $adminRoute = $request->get('_route');

        $route = $this->routeManager->getRouteFromAdminRoute($adminRoute);
        $title = $this->routeManager->getTitle($route); 
        $fields = $this->routeManager->getAdminFields($route);
        $seoMeta = $this->routeManager->getSeoMeta($route);

        $seoForm = $this->createForm(new SeoMetaType(), $seoMeta);
        $seo = true;

        $data = array(
            'title' => $title,
            'route' => $route,
            'admin_route' => $adminRoute,
            'fields' => $fields,
            'seo_form' => $seoForm->createView(),
            'seo' => $seo,
        );

        return $this->render(
            $this->contentView, 
            $data
        );

    }


    public function contentUpdateAction(Request $request, $adminRoute, $route)
    {
        $params = $request->request->all();
        $flash = $this->container->get('session')->getFlashBag();

        foreach ($params as $param => $p) {
            $param = str_replace('_', ' ', $param);
            $entity = $this->routeRepo->findOneBy(array(
               'metaName' => $param,
               'route' => $route,
            ));
            if (!$entity) {
                $flash->add('error', 'Fields update failed!');
                throw $this->createNotFoundException('Unable to find RouteMeta entity.');
            }

            $entity->setMetaValue($p);
            $this->em->flush();
        }

        $flash->add('success', 'Fields updated successfully!');

        $redirect = $this->container->get('router')->generate($adminRoute);
        return new RedirectResponse($redirect);
    }



    public function seoUpdateAction(Request $request, $adminRoute, $route)
    {
        $flash = $this->container->get('session')->getFlashBag();
        $entity = $this->seoRepo->findOneBy(array('route' => $route));

        if (!$entity) {
            $entity = new SeoMeta();
        }

        $seoForm = $this->createForm(new SeoMetaType(), $entity);
        $seoForm->bind($request);

        if ($seoForm->isValid()) {
            $this->em->persist($entity);
            $this->em->flush();

            $flash->add('success', 'SEO updated successfully!');

            $redirect = $this->container->get('router')->generate($adminRoute);
            return new RedirectResponse($redirect);
        }

        $flash->add('error', 'SEO update failed!');

        $redirect = $this->container->get('router')->generate($adminRoute);
        return new RedirectResponse($redirect);

    }

}
