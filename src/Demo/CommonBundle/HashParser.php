<?php

namespace Demo\CommonBundle;

use Doctrine\ORM\EntityManager;

use DateTime;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Demo\CommonBundle\Entity\Address;
use Demo\CommonBundle\Entity\Property;
use Demo\CommonBundle\Entity\IhgProperty;
use Demo\CommonBundle\Entity\CombinedListing;
use Demo\CommonBundle\Entity\ImporterReport;

class HashParser
{
    // Importer Data
    private $name = 'AddressHash';
    private $startTime;
    private $report;
    
    // Entities
    private $addressRepo;
    private $propertyRepo;
    private $ihgRepo;
    private $combinedRepo;
    private $importerRepo;

    protected $em;
    protected $hashless;
    protected $duplicateHashes;
    protected $batch = 0;
    protected $totalHashed = 0;

    protected $importerClass = 'Demo\CommonBundle\Entity\ImporterReport'; 

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->addressRepo = $this->em->getRepository('CommonBundle:Address');
        $this->propertyRepo = $this->em->getRepository('CommonBundle:Property');
        $this->ihgRepo = $this->em->getRepository('CommonBundle:IhgProperty');
        $this->combinedRepo = $this->em->getRepository('CommonBundle:CombinedListing');
        $this->importerRepo = $this->em->getRepository('CommonBundle:ImporterReport');
    }
    
    public function clearMemory()
    {
        $this->em->flush();
        $this->em->clear();
    }

    public function startImporter()
    {
        $this->startTime = microtime(true);
        $oldLatest = $this->importerRepo->findOneBy(array('isLatest' => 1, 'name' => $this->name));
        if ($oldLatest) {
            if ($oldLatest->getStatus() == 'Started') {
                $oldLatest->setStatus('Failed');
            }
            $oldLatest->setIsLatest(0);
            $this->em->flush();
        }
        $this->report = new $this->importerClass;
        $this->report->setName($this->name);
        $this->report->setStatus('Started');
        $this->report->setDuration(1);

        $this->report->setIsLatest(1);
        $this->report->setReportDate(new \DateTime('now'));
        $this->em->persist($this->report);
        $this->em->flush();
    }

    public function stopImporter($log)
    {
        $duration = (microtime(true) - $this->startTime);
        $this->report->setDuration($duration);
        $this->report->setLog($log);
        $this->report->setStatus('Complete');
        $this->em->persist($this->report);
        $this->em->flush();
    }


    public function getHashless()
    {
        $this->hashless = $this->addressRepo->findBy(array(
            'hash' => null,         
        ));
        return count($this->hashless);
    }

    public function updateHash()
    {
        foreach ($this->hashless as $h) {
            $h->setLine1(trim($h->getLine1()));
            $h->setLine2(trim($h->getLine2()));
            $h->setCity(trim($h->getCity()));
            $h->setState(trim($h->getState()));
            $h->setZip(trim($h->getZip()));
            $h->setCountry(trim($h->getCountry()));
            $hash = $h->generateHash();

            $hashed = $this->addressRepo->findOneBy(array(
                'hash' => $hash,          
            ));

            if ($hashed) {
                $this->duplicateHashes = [];
                $this->duplicateHashes[$hash] = array('hash' => $h, 'newer_hash' => $hashed);
                $this->updateDuplicates();
                $this->totalHashed++;
            } else {
                $h->updateHash();
                $this->em->flush();
                $this->totalHashed++;
            }
        }
        return $this->totalHashed;

    }

    private function updateDuplicates()
    {
        foreach ($this->duplicateHashes as $dup ) {
            $propAddress = $this->propertyRepo->findBy(array(
                'address' => $dup['hash'],            
            ));
            if ($propAddress) {
                foreach($propAddress as $add) {
                    $add->setAddress($dup['newer_hash']);
                    $this->em->persist($add);
                }
            }
            $propBilling = $this->propertyRepo->findBy(array(
                'billingAddress' => $dup['hash'],          
            ));
            if ($propBilling) {
                foreach($propBilling as $bill) {
                    $bill->setBillingAddress($dup['newer_hash']);
                    $this->em->persist($bill);
                }
            }
            $ihgProp = $this->ihgRepo->findBy(array(
                'address' => $dup['hash'],              
            ));
            if ($ihgProp) {
                foreach($ihgProp as $ihg) {
                    $ihg->setAddress($dup['newer_hash']);
                    $this->em->persist($ihg);
                }
            }
            $combined = $this->combinedRepo->findBy(array(
                'address' => $dup['hash'],              
            ));
            if ($combined) {
                foreach($combined as $c) {
                    $c->setAddress($dup['newer_hash']);
                    $this->em->persist($c);
                }
            }

            $this->em->remove($dup['hash']);
            $this->em->flush();
        }

    }

}
