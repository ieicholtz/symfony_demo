<?php

namespace Demo\CommonBundle;

use DateTime;

use Doctrine\ORM\EntityManager;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Demo\CommonBundle\Entity\Property;
use Demo\CommonBundle\Entity\PropertyAdvertisement;
use Demo\CommonBundle\Entity\Contract;
use Demo\CommonBundle\Entity\PropertyReport;
use Demo\CommonBundle\Entity\Address;
use Demo\CommonBundle\Entity\ImporterReport;

class ReportParser
{
    // Importer Data
    private $name = 'Reports';
    private $startTime;
    private $report;

    // Entities
    private $propertyRepo;
    private $advertisementRepo;
    private $contractRepo;
    private $reportRepo;
    private $addressRepo;
    private $importerRepo;

    // Entity Classes
    protected $reportClass = 'Demo\CommonBundle\Entity\PropertyReport';

    protected $em;
    protected $properties;
    protected $ads;
    protected $addresses;
    protected $results = [];
    protected $newReports = [];
    protected $property;

    protected $importerClass = 'Demo\CommonBundle\Entity\ImporterReport'; 

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->propertyRepo = $this->em->getRepository('CommonBundle:Property');
        $this->advertisementRepo = $this->em->getRepository('CommonBundle:PropertyAdvertisement');
        $this->contractRepo = $this->em->getRepository('CommonBundle:Contract');
        $this->reportRepo = $this->em->getRepository('CommonBundle:PropertyReport');
        $this->addressRepo = $this->em->getRepository('CommonBundle:Address');
        $this->importerRepo = $this->em->getRepository('CommonBundle:ImporterReport');
    }

    public function clearMemory()
    {
        $this->properties = [];
        $this->ads = [];
        $this->adresses = [];
        $this->results = [];
        $this->newReports = [];
        $this->em->flush();
        $this->em->clear();
    }

    public function startImporter()
    {
        $this->startTime = microtime(true);
        $oldLatest = $this->importerRepo->findOneBy(array('isLatest' => 1, 'name' => $this->name));
        if ($oldLatest) {
            if ($oldLatest->getStatus() == 'Started') {
                $oldLatest->setStatus('Failed');
            }
            $oldLatest->setIsLatest(0);
            $this->em->flush();
        }
        $this->report = new $this->importerClass;
        $this->report->setName($this->name);
        $this->report->setStatus('Started');
        $this->report->setDuration(1);

        $this->report->setIsLatest(1);
        $this->report->setReportDate(new \DateTime('now'));
        $this->em->persist($this->report);
        $this->em->flush();
    }

    public function stopImporter($log)
    {
        $duration = (microtime(true) - $this->startTime);
        $this->report->setDuration($duration);
        $this->report->setLog($log);
        $this->report->setStatus('Complete');
        $this->em->persist($this->report);
        $this->em->flush();
    }


    public function cacheProperties()
    {
        $this->properties = $this->propertyRepo->createQueryBuilder('p')
            ->select('p.id', 'p.accountNumber', 'IDENTITY(p.address)')
            ->getQuery()
            ->getResult();

        return count($this->properties);
    }

    public function cacheAds()
    {
        $this->ads = $this->advertisementRepo->createQueryBuilder('a')
            ->select('DISTINCT(a.property)')
            ->getQuery()
            ->getResult();

        return count($this->ads);
    }

    public function cacheAddresses()
    {
        $this->addresses = $this->addressRepo->createQueryBuilder('a')
            ->select('a.id','a.latitude', 'a.longitude')
            ->where ('a.latitude = :value')
            ->orWhere ('a.longitude = :value')
            ->orWhere ('a.latitude IS NULL')
            ->orWhere ('a.longitude IS NULL')
            ->setParameter('value', '')
            ->getQuery()
            ->getResult();

        return count($this->addresses);
    }


    public function checkReports()
    {
        $sortAds = [];
        foreach ($this->ads as $a) {
            $sortAds[$a[1]] = $a;
        }

        $sortAddresses = [];
        foreach ($this->addresses as $ad) {
            $sortAddresses[$ad['id']] = array(
                'lat' => $ad['latitude'],
                'long' => $ad['longitude'],
            );
        }

        foreach ($this->properties as $prop) {
            $id = $prop['id'];
            $address = $prop[1];
            $setData = false;
            $data = array(
                'id' => $id,
                'account' => $prop['accountNumber'],
                'rate' => null,
                'geocode' => null,
            );
            if (!isset($sortAds[$id])) {
                $data['rate'] = 'Has No advertisements';
                $setData = true;
            }

            if (isset($sortAddresses[$address])) {
                $data['geocode'] = 'Missing Latitude and/or Longitude';
                $setData = true;
            }
            if ($setData) {
                $this->results[] = $data;
            }
        }

        return count($this->results);
    }

    public function saveReports()
    {
        foreach ($this->results as $r) {
            $report = $this->reportRepo->findOneBy(array('property' => $r['id']));

            if ($report) {
                $report->setRate($r['rate']);
                $report->setGeocode($r['geocode']);
                $report->setReportDate(new \DateTime('now'));
            } else {
                $report = new $this->reportClass;
                $report->setProperty($r['id']);
                $report->setAccountNumber($r['account']);
                $report->setRate($r['rate']);
                $report->setGeocode($r['geocode']);
                $report->setReportDate(new \DateTime('now'));
                $this->em->persist($report);
            }
            $this->newReports[$report->getId()] = true;
            $this->em->flush();
        }
    }

    public function removeOldReports()
    {
        $oldReports = $this->reportRepo->findAll();
        foreach ($oldReports as $r) {
            if (!isset($this->newReports[$r->getId()])) {
                $this->em->remove($r);
                $this->em->flush();
            }
        }
    }

}
