<?php

namespace Demo\CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BadgeFeedCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('feed:badges')
            ->setDescription('Update Badges');
    }

    protected function bail($code = 230)
    {
        exit($code);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $parser = $this->getContainer()->get('sym_example.badgeparser');
        $parser->BadgesInit();

        $output->writeln('<comment>Begin First Badge Update:</comment>');

        $output->writeln('<comment>Updating Badges:</comment>');
        $updateBadges = $parser->updateFirstBadge();
        if ($updateBadges) {
            $output->writeln('<fg=green>'.$updateBadges.'</fg=green>');
        } else {
            $output->writeln('<error>Failed to update first badges</error>');
            $this->bail();
        }

        $output->writeln('<fg=green>Done!</fg=green>');


        $output->writeln('<comment>Begin Second Badge Update:</comment>');

        $output->writeln('<comment>Updating Badges:</comment>');
        $updateBadges = $parser->updateSecondBadge();
        if ($updateBadges) {
            $output->writeln('<fg=green>'.$updateBadges.'</fg=green>');
        } else {
            $output->writeln('<error>Failed to update second badges</error>');
            $this->bail();
        }

        $output->writeln('<fg=green>Done!</fg=green>');


        $output->writeln('<comment>Begin Third Badge Update:</comment>');

        $output->writeln('<comment>Updating Badges:</comment>');
        $updateBadges = $parser->updateThirdBadge();
        if ($updateBadges) {
            $output->writeln('<fg=green>'.$updateBadges.'</fg=green>');
        } else {
            $output->writeln('<error>Failed to update third badges</error>');
            $this->bail();
        }

        $output->writeln('<fg=green>Done!</fg=green>');


        $output->writeln('<comment>Begin Fourth Badge Update:</comment>');

        $output->writeln('<comment>Updating Badges:</comment>');
        $updateBadges = $parser->updateFourthBadge();
        if ($updateBadges) {
            $output->writeln('<fg=green>'.$updateBadges.'</fg=green>');
        } else {
            $output->writeln('<error>Failed to update fourth badges</error>');
            $this->bail();
        }

        $output->writeln('<fg=green>Done!</fg=green>');




        $this->bail(0);

    }
}
