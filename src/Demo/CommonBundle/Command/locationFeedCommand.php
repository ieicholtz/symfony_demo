<?php

namespace Demo\CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LocationFeedCommand extends ContainerAwareCommand
{
    protected $feedFilePath;

    protected function configure()
    {
        $this
            ->setName('location:feed')
            ->setDescription('Get Latest Location Feed');
    }

    protected function bail($code = 230)
    {
        unlink($this->feedFilePath);
        exit($code);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln('<comment>Saving Feed:</comment>');

        $feedUrl  = '';
        $now      = strtotime('now');
        $filename = "example$now";

        $this->feedFilePath = $filepath = "/tmp/$filename";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $feedUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch); 
        
        if (!$data) {
            $output->writeln('<error>unable to retrieve feed data</error>');
            $this->bail();
        }

        $document = new \DOMDocument();
        $document->formatOutput = true;

        $document->loadXml($data);

        $file = fopen($filepath, 'w');
        fwrite($file, $document->saveXml());
        fclose($file); 

        $parser = $this->getContainer()->get('sym_example.parser');

        $output->writeln("<fg=green>Feed saved to $filepath</fg=green>");

        $output->writeln('<comment>Parsing Feed:</comment>');

        $list = $parser->parseFileToEntity($filepath);
        if (sizeOf($list) > 1) {
            $output->writeln('<fg=green>Success!</fg=green>');

            // setFranchiseUpdated
            $output->writeln('<comment>Resetting updated value for Franchise entity:</comment>');
            $fUpdated = $parser->setFranchiseUpdated();
            if ($fUpdated) {
                $output->writeln('<fg=green>'.$fUpdated.'</fg=green>');
            } else {
                $output->writeln('<error>Franchise updated reset failed</error>');
                $this->bail();
            }
             
            //populateFranchiseEntity 
            $output->writeln('<comment>Populating Franchise entity:</comment>');
            $setFranchise = $parser->populateFranchiseEntity($list);

            if ($setFranchise) {
                $output->writeln('<fg=green>'.$setFranchise.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to populate Franchise entity</error>');
                $this->bail();
            }

            //checkFranchiseUpdated 
            $output->writeln('<comment>Removing unused records from Franchise entity:</comment>');
            $fCheck = $parser->checkFranchiseUpdated();
            if ($fCheck) {
                $output->writeln('<fg=green>'.$fCheck.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to remove records from Franchise entity</error>');
                $this->bail();
            }

            //setContactsUpdated
            $output->writeln('<comment>Resetting updated value for LocationContacts entity:</comment>');
            $cUpdated = $parser->setContactsUpdated();
            if ($cUpdated) {
                $output->writeln('<fg=green>'.$cUpdated.'</fg=green>');
            } else {
                $output->writeln('<error>LocationContacts updated reset failed</error>');
                $this->bail();
            }
                     
            //populateContactsEntity 
            $output->writeln('<comment>Populating LocationContacts entity:</comment>');
            $setContacts = $parser->populateContactsEntity($list);
            if ($setContacts) {
                $output->writeln('<fg=green>'.$setContacts.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to populate LocationContacts entity</error>');
                $this->bail();
            }

            //checkContactsUpdated 
            $output->writeln('<comment>Removing unused records from LocationContacts entity:</comment>');
            $cCheck = $parser->checkContactsUpdated();
            if ($cCheck) {
                $output->writeln('<fg=green>'.$cCheck.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to remove records from LocationContacts entity</error>');
                $this->bail();
            }

            //setAreasUpdated
            $output->writeln('<comment>Resetting updated value for ServiceAreas entity:</comment>');
            $aUpdated = $parser->setAreasUpdated();
            if ($aUpdated) {
                $output->writeln('<fg=green>'.$aUpdated.'</fg=green>');
            } else {
                $output->writeln('<error>ServiceAreas updated reset failed</error>');
                $this->bail();
            }

            //populateAreasEntity 
            $output->writeln('<comment>Populating ServiceAreas entity:</comment>');
            $setAreas = $parser->populateAreasEntity($list);
            if ($setAreas) {
                $output->writeln('<fg=green>'.$setAreas.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to populate ServiceAreas entity</error>');
                $this->bail();
            }

            //checkAreasUpdated 
            $output->writeln('<comment>Removing unused records from ServiceAreas entity:</comment>');
            $aCheck = $parser->checkAreasUpdated();
            if ($aCheck) {
                $output->writeln('<fg=green>'.$aCheck.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to remove records from ServiceAreas entity</error>');
                $this->bail();
            }

            // setReviewsUpdated
            $output->writeln('<comment>Resetting updated value for Review entity:</comment>');
            $rUpdated = $parser->setReviewsUpdated();
            if ($rUpdated) {
                $output->writeln('<fg=green>'.$rUpdated.'</fg=green>');
            } else {
                $output->writeln('<error>Review updated reset failed</error>');
                $this->bail();
            }

            //populateReviewsEntity 
            $output->writeln('<comment>Populating Reviews entity:</comment>');
            $setReviews = $parser->populateReviewsEntity($list);
            if ($setReviews) {
                $output->writeln('<fg=green>'.$setReviews.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to populate Reviews entity</error>');
                $this->bail();
            }

            //checkReviewsUpdated 
            $output->writeln('<comment>Removing unused records from Reviews entity:</comment>');
            $rCheck = $parser->checkReviewsUpdated();
            if ($rCheck) {
                $output->writeln('<fg=green>'.$rCheck.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to remove records from Reviews entity</error>');
                $this->bail();
            }

            // setPromotionsUpdated
            $output->writeln('<comment>Resetting updated value for Promotions entity:</comment>');
            $pUpdated = $parser->setPromotionsUpdated();
            if ($pUpdated) {
                $output->writeln('<fg=green>'.$pUpdated.'</fg=green>');
            } else {
                $output->writeln('<error>Promotion updated reset failed</error>');
                $this->bail();
            }

            //populatePromotionsEntity 
            $output->writeln('<comment>Populating Promotion entity:</comment>');
            $setPromotions = $parser->populatePromotionsEntity($list);
            if ($setPromotions) {
                $output->writeln('<fg=green>'.$setPromotions.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to populate Promotions entity</error>');
                $this->bail();
            }

            //checkPromotionsUpdated 
            $output->writeln('<comment>Removing unused records from Promotions entity:</comment>');
            $pCheck = $parser->checkPromotionsUpdated();
            if ($pCheck) {
                $output->writeln('<fg=green>'.$pCheck.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to remove records from Promotions entity</error>');
                $this->bail();
            }

            //removeJobListings 
            $output->writeln('<comment>Removing non-templated job listings</comment>');
            $jUpdated = $parser->removeJobListings();
            if ($jUpdated) {
                $output->writeln('<fg=green>'.$jUpdated.'</fg=green>');
            } else {
                $output->writeln('<error>Job listing removal failed</error>');
                $this->bail();
            }

            //populateJobEntity
            $output->writeln('<comment>Populating Job entity:</comment>');
            $setJobs = $parser->populateJobEntity($list);
            if ($setJobs) {
                $output->writeln('<fg=green>'.$setJobs.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to populate Job entity</error>');
                $this->bail();
            }

            // setAliasUpdated
            $output->writeln('<comment>Resetting updated value for Automated Aliases:</comment>');
            $amUpdated = $parser->setAliasUpdated();
            if ($amUpdated) {
                $output->writeln('<fg=green>'.$amUpdated.'</fg=green>');
            } else {
                $output->writeln('<error>Automated Aliases updated reset failed</error>');
                $this->bail();
            }
            
            //populateAliasUpdated 
            $output->writeln('<comment>Generating Automated Aliases:</comment>');
            $setAliases = $parser->populateAliasEntity($list);
            if ($setAliases) {
                $output->writeln('<fg=green>'.$setAliases.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to Automates Aliases</error>');
                $this->bail();
            }

            //checkAliasUpdated 
            $output->writeln('<comment>Removing unused records from Automated Aliases:</comment>');
            $amCheck = $parser->checkAliasUpdated();
            if ($amCheck) {
                $output->writeln('<fg=green>'.$amCheck.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to remove records from Automated Aliases</error>');
                $this->bail();
            }


            // setStateAliasUpdated
            $output->writeln('<comment>Resetting updated value for State Aliases:</comment>');
            $saUpdated = $parser->setStateAliasUpdated();
            if ($saUpdated) {
                $output->writeln('<fg=green>'.$saUpdated.'</fg=green>');
            } else {
                $output->writeln('<error>State Aliases updated reset failed</error>');
                $this->bail();
            }

            //checkAliasUpdated 
            $output->writeln('<comment>Removing unused records from State Aliases:</comment>');
            $amCheck = $parser->checkStateAliasUpdated();
            if ($amCheck) {
                $output->writeln('<fg=green>'.$amCheck.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to remove records from State Aliases</error>');
                $this->bail();
            }

            
            //populateStateAliases 
            $output->writeln('<comment>Populating State Aliases:</comment>');
            $setAliases = $parser->populateStateAliases();
            if ($setAliases) {
                $output->writeln('<fg=green>'.$setAliases.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to Automates Aliases</error>');
                $this->bail();
            }

            //checkAliasUpdated 
            $output->writeln('<comment>Removing unused records from State Aliases:</comment>');
            $amCheck = $parser->checkStateAliasUpdated();
            if ($amCheck) {
                $output->writeln('<fg=green>'.$amCheck.'</fg=green>');
            } else {
                $output->writeln('<error>Failed to remove records from State Aliases</error>');
                $this->bail();
            }




        } else {
            $output->writeln('<error>Parsing Failed</error>');
            $this->bail();
        }

        $output->writeln('<fg=green>Done!</fg=green>');

        $this->bail(0);
    }
}
