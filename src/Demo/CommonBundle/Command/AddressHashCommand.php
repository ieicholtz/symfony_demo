<?php
namespace Demo\CommonBundle\Command;

use DateTime;
use SimpleXMLElement;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AddressHashCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('data:fixhash')
            ->setDescription('Set a hash on all addresses that don\'t have one');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $log = '';

        $this->parser = $this->getContainer()->get('hash_parser');
        $this->parser->startImporter();

        // Get Total Hashless Records
        $totalHashless = $this->parser->getHashless();

        if ($totalHashless > 0) {
            $totalHashed = $this->parser->updateHash();
            $log = $totalHashed.'Addresses Hashed';
        } else {
            $log = 'No Addresses Need Hashed';
        }

        $this->parser->stopImporter($log);
        $this->parser->clearMemory();

    }
}
