<?php
namespace Demo\CommonBundle\Command;

use DateTime;
use SimpleXMLElement;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('data:reports')
            ->setDescription('Update Reports for invalid properties');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $log = '';

        $this->parser = $this->getContainer()->get('report_parser');
        $this->parser->startImporter();

        // Cache Properties
        $totalProperties = $this->parser->cacheProperties();

        // Cache Ads
        $totalAds = $this->parser->cacheAds();

        // Cache Addresses
        $totalAddresses = $this->parser->cacheAddresses();

        // Find Reports 
        $totalResults = $this->parser->checkReports();
        
        if ($totalResults > 0) {
            $log = $totalResults.' Issues Reported';
        } else {
            $log = 'Nothing to Report';
        }

        // Save Reports 
        $this->parser->saveReports();

        // Remove Old Reports 
        $this->parser->removeOldReports();

        $this->parser->stopImporter($log);
        $this->parser->clearMemory();

    }
}
