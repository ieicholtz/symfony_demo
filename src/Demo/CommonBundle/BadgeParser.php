<?php 

namespace Demo\CommonBundle;

use Doctrine\ORM\EntityManager;

use SYM\ExampleBundle\Entity\Widget;
use SYM\ExampleBundle\Entity\WidgetBadges;
use SYM\ExampleBundle\Entity\Example;
use SYM\ExampleBundle\Entity\ExampleProfile;

use Guzzle\Http\Client; 

class BadgeParser
{
    protected $em;
    protected $batchSize = 40;
    protected $ticker = 0;
    protected $widgetRepo;
    protected $badgeRepo;
    protected $exampleRepo;
    protected $exampleProfRepo;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function initBatch()
    {
        $this->ticker = 0;
    }

    protected function persist($entity)
    {
        $this->em->persist($entity);

        echo '.';

        if (++$this->ticker % $this->batchSize === 0) {
            $this->em->flush();
        }
    }

    public function BadgesInit()
    {
        $this->widgetRepo    = $this->em->getRepository('SYMExampleBundle:Widget'); 
        $this->badgeRepo     = $this->em->getRepository('SYMExampleBundle:WidgetBadges'); 
        $this->exampleRepo   = $this->em->getRepository('SYMExampleBundle:Example'); 
        $this->exampleProfRepo   = $this->em->getRepository('SYMExampleBundle:ExampleProfile'); 

    }
    
    public function updateFirstBadge()
    {
        $this->initBatch();

        $widgets = $this->widgetRepo->findAll();
        $examples = $this->exampleRepo->findAllByStatus('complete');
        foreach ($widgets as $widget) {
            $badges =  $this->badgeRepo->findOneBy(array('widgetId' => $widget->getId()));
            if (!$badges) {
                $badge = new WidgetBadges();
                $badges->setWidget($widget);
            }
            $amount = 0;
            foreach ($examples as $example) {
               if ($example->getWidgetId() == $widget->getId()) {
                    $exampleAmount = (int) $example->getAmount();
                    $amount = $amount+$exampleAmount;
               }

            }
            $badges->setFirstExample(0);
            $badges->setExample1k(0);
            $badges->setExample2k(0);
            $badges->setExample5k(0);

            if ($amount) {
                $badges->setFirstExample(1);

                if ($amount >= 1000 && $amount <= 1999) {
                    $badges->setExample1k(1);
                }
                if ($amount >= 2000 && $amount <= 4999) {
                    $badges->setExample2k(1);
                }
                if ($amount >= 5000) {
                    $badges->setExample5k(1);
                }
            } 

            $this->persist($badges);


        }
        echo PHP_EOL;
        return 'Success!';
 

    }

    public function updateSecondBadge()
    {
        $this->initBatch();

        $widgets = $this->widgetRepo->findAll();
        $profiles = $this->exampleProfRepo->findAll();
        $currentYear = new \DateTime();
        $currentYear = $currentYear->format('Y');
        foreach ($widgets as $widget) {
            $badges =  $this->badgeRepo->findOneBy(array('widgetId' => $widget->getId()));
            if (!$badges) {
                $badge = new WidgetBadges();
                $badges->setWidget($widget);
            }
            $hasBadge = 0;
            $year = '';
            foreach ($profiles as $prof) {
               if ($prof->getWidgetId() == $widget->getId()) {
                    $year = $prof->getIncorporationYear(); 
               }

            }
            if ($year) {
                $age = $currentYear - $year;
                if ($age >= 25) {
                    $hasBadge = 1;
                }
            }
            $badges->setSecond($hasBadge);
            $this->persist($badges);


        }
        echo PHP_EOL;
        return 'Success!';
    }


    public function updateThirdBadge()
    {
        $client = new Client(); 
        $gsQuery = 'http://example.org/widgets/browse/search:%s';

        $this->initBatch();

        $widgets = $this->widgetRepo->findAll();
        foreach ($widgets as $widget) {
            $badges =  $this->badgeRepo->findOneBy(array('widgetId' => $widget->getId()));

            $gsUrl = sprintf($gsQuery . PHP_EOL, $widget->getEin());
            $gsRequest = $client->get($gsUrl);

            try {
                $gsResponse = $gsRequest->send();
            }
            catch (\Exception $e) {
                return $e;
            }

            $code = $gsResponse->getStatusCode();
            if (!$badges) {
                $badge = new WidgetBadges();
                $badges->setWidget($widget);
            }
            $hasBadge = 0;
            if ($code == '200') {
                $hasBadge = 1;   
            }
            $badges->setThird($hasBadge);
            $this->persist($badges);


        }
        echo PHP_EOL;
        return 'Success!';

    }

    public function updateFourthBadges()
    {
        $client = new Client(); 
        $apiKey = 'abcdefghijklmnopqrstuv'; 
        $apiId = '1234567890';
        $cnQuery = 'http://api.example.org/api/v1/search/?app_key='.$apiKey.'&app_id='.$apiId.'&format=json&ein=%s';

        $this->initBatch();

        $widgets = $this->widgetRepo->findAll();
        foreach ($widgets as $widget) {
            $badges =  $this->badgeRepo->findOneBy(array('widgetId' => $widget->getId()));

            $ein = $widget->getEin();
            $ein = preg_replace('/-/','', $ein);

            $cnUrl = sprintf($cnQuery . PHP_EOL, $ein);
            $cnRequest = $client->get($cnUrl);

            try {
                $cnResponse = $cnRequest->send();
            }
            catch (\Exception $e) {
                return $e;
            }

            $response = $cnResponse->json();
            $count = $response['meta']['total_count'];

            if (!$badges) {
                $badge = new WidgetBadges();
                $badges->setWidget($widget);
            }
            $hasBadge = 0;
            if ($count >= 1) {
                $hasBadge = 1;   
            }
            $badges->setFourth($hasBadge);
            $this->persist($badges);


        }
        echo PHP_EOL;
        return 'Success!';

    }


}
