<?php

namespace Demo\CommonBundle;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class Utils
{    
    protected $em;
    protected $container;

    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * Set Cookie For First Time Visitor
     *
     */
    public function handleCookie()
    {
        $cookie = new Cookie('first_visit', 'true', time() + 3600 * 24 * 7);
        $response = new Response();
        $response->headers->setCookie($cookie);
        return $response->send();
    }


}

