<?php

namespace Demo\CommonBundle;

use Doctrine\ORM\EntityManager;

use Demo\AdminBundle\Entity\RouteMeta;
use Demo\AdminBundle\Entity\SeoMeta;

class RouteManager
{
    protected $em;
    protected $routeRepo;
    protected $seoRepo;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->routeRepo = $this->em->getRepository('DemoAdminBundle:RouteMeta');
        $this->seoRepo = $this->em->getRepository('DemoAdminBundle:SeoMeta');
    }

    // Get Route From admin_route
    public function getRouteFromAdminRoute($adminRoute)
    {
        $route = $this->routeRepo->findRouteByAdminMetaValue($adminRoute);
        
        if (!$route) {
                throw $this->createNotFoundException('Admin Route Meta Value not set');
        };
        
        return $route['route'];
    }
    
    // Get Page Title
    public function getTitle($route)
    {
        $title = $this->routeRepo->findPageTitleByRoute($route);
        
        if (!$title) {
                throw $this->createNotFoundException('Page title not set');
        };
        
        return $title['metaValue'];
    }
    
    // Get Meta Fields
    public function getAdminFields($route)
    {
        $entities = $this->routeRepo->findByRoute($route);

        $fields = array();
        foreach ($entities as $entity) {
            $metaName = $entity->getMetaName();
            if($metaName == 'admin_route' || $metaName == 'page_title') {
            }else{
                $fields[] = $entity;
            }
        }

        return $fields;

    }

    // get SEO Fields
    public function getSeoMeta($route)
    {
        $seo = $this->seoRepo->findOneBy(array('route' => $route));

        $fields = array();
        if ($seo) {
            $fields = $seo;
        } else {
            $fields = new SeoMeta();
            $fields->setRoute($route);
        }
        
        return $fields;
    }

    // Get Content Fields
    public function getFields($route)
    {
        $routesList = $this->routeRepo->findByRoute($route);

        $fields = array();
        foreach ($routesList as $r) {
            $fName = preg_replace('/[^a-z0-9]/i', '_', strtolower($r->getMetaName()));
            $fName = preg_replace('/_+/', '_', $fName);
            $fName = trim($fName , '_');

            if ($r->getMetaType() == 'video') {
                $fields[$fName] = static::formatVideo($r->getMetaValue());
            } else {
                $fields[$fName] = $r->getMetaValue();
            }
        }

        return $fields;
    }

    // get SEO Content Fields
    public function getSeoFields($route)
    {
        $seoList = $this->seoRepo->findOneBy(array('route' => $route));
        if($seoList) {
            $fields = $seoList;
        } else {
            $fields = array(
                'seoTitle' => '',
                'seoKeywords' => '',
                'seoDescription' => '',
            );
        }

        return $fields;
    }

    /**
     * Formats a youtube video to proper embed
     *
     * @param string $video
     * @return string 
     */
    private static function formatVideo($video)
    {
        $video = rtrim($video, '&feature=youtu.be');
        $video = str_replace('watch?v=', 'embed/', $video); 
        return $video;
    }


}
