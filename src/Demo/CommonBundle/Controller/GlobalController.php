<?php

namespace Demo\CommonBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bridge\Monolog\Logger;


class GlobalController extends ContainerAware
{
    protected $em;
    protected $container;
    protected $templating;
    protected $viewData;
    protected $logger;
    protected $utils;
    protected $routeManager;
    protected $seoFields = '';
    protected $fields = '';

    /**
     * routes
     */
    protected $indexRoute;
    protected $newRoute;
    protected $editRoute;
    protected $updateRoute;
    protected $deleteRoute;
    protected $viewRoute;
    protected $seoRoute;
    protected $reorderRoute;

    /**
     * Naming Variables
     **/
    protected $entityName;
    protected $entitySingularName;
    protected $entitySelector;

    /**
     * Views
     **/
    protected $indexView;
    protected $newView;
    protected $editView;

    /** 
     * Form Class 
     **/
    protected $formClass;

    /** 
     * Entity Class 
     **/
    protected $entityClass;

    public function __construct(Logger $logger, EntityManager $em, Container $container, $templating)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->templating = $templating;
        $this->container = $container;
        $this->utils = $this->container->get('demo.utils');
        $this->routeManager = $this->container->get('demp.routeManager');
        $this->initialize();
    }

    public function initialize() {}

    public function render($view, array $parameters = array(), Response $response = null)
    {
        return $this->templating->renderResponse($view, $parameters, $response);
    }

    public function createNotFoundException($message = 'Not Found', \Exception $previous = null)
    {
        return new NotFoundHttpException($message, $previous);
    }

    public function createForm($type, $data = null, array $options = array())
    {
        return $this->container->get('form.factory')->create($type, $data, $options);
    }

    public function createDeleteForm($id)
    {
        return $this->container->get('form.factory')->createBuilder('form', array('id'=>$id))
            ->add('id', 'hidden')
            ->getForm();
    }

    protected function storeRoute() {
        $this->container->get('session')->set('last_url', $this->container->get('request')->getUri());
    }

    public function forward($controller, array $path = array(), array $query = array())
    {
        $path['_controller'] = $controller;
        $subRequest = $this->container->get('request')->duplicate($query, null, $path);

        return $this->container->get('http_kernel')->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
    }

    /**
     * Initializes view data for the current route
     */
    public function initViewData($route = null, $slug=null)
    {
        if(isset($route)) {
            $routeName = $route;
        } else {
            $request   = $this->container->get('request');
            $routeName = $request->get('_route');
        }

        if (!isset($slug)) {
            $slug = $routeName;
        }

        $this->seoFields =  $this->getSeoFields($slug);
        $this->fields = $this->getFields($routeName);
    }

    public function viewData()
    {
        return array(
            'seoFields' => $this->seoFields,
            'fields' => $this->fields,
        );
    }

    public function adminViewData()
    {
        $data = array();
        $data['index_route'] = $this->indexRoute;
        $data['new_route'] = $this->newRoute;
        $data['edit_route'] = $this->editRoute;
        $data['update_route'] = $this->updateRoute;
        $data['delete_route'] = $this->deleteRoute;
        $data['view_route'] = $this->viewRoute;
        $data['seo_route'] = $this->seoRoute;
        $data['entity_name'] = $this->entityName;
        $data['entity_single'] = $this->entitySingularName;
        $data['entity_selector'] = $this->entitySelector;
        return $data;

    }

    /* Get Admin Fields */
    private function getFields($route)
    {
        $fields = $this->routeManager->getFields($route);

        return $fields;
    }

    /* Get SEO Fields */
    private function getSeoFields($route)
    {
        $fields = $this->routeManager->getSeoFields($route);

        return $fields;
    }




}
